# Insta-response

*Instaclustr Incident Response System*: A web based tool for managing critical impact incidents.

### Description:
Currently, Slack channel is used to notify critical impact incidents with the following drawbacks: -
- Cumbersome and error prone.
- Difficult to see who is involved.
- Difficult to see the impacts of the issue.
- Difficult to follow whilst the issue is in flight.
- Difficult to see the status of an incident across the organisation.
- Difficult to involve bring in / inform the appropriate team members.

##### Goals and how it will solve the problem:
* Provide a unified view of an incident via a web-based tool which centralises all
information related to the critical incident including
  - ZenDesk tickets
  - Jira tickets
  - Status page
  - Status updates
  - Postmortem etc.  
* Anyone at Instaclustr can prompt a critical incident response at any time via 
web portal.
* Citadel on-call or VP or CPO will look at the impact and initiate a response 
if appropriate by allocating an existing incident manager via the set procedure.
* Once an incident has begun, a single link to the response page will be put into
the form for the relevant teams to follow along (CoC & TechOps on-call PDs will be alerted
of the issue and be able to follow along with the provided link). 

### Deliverables:
A web-based tool which will automate Instaclustr's existing incident response
procedures.

### Spring Setup (OSX):
- `Jdk version` from [Spring Initializr](start.spring.io) - 11

- `Jdk version` on Intellij - 13.0.2  *(For some reason, it is working now ;). We can change that later in case we 
get any compatibility issues with jdk versions.)*

- `Apache Tomcat version` - [Tomcat 9.0.52](tomcat.apache.org) 
- **Add Tomcat on Intellij**: -
  1. Go to Intellij Preferences (`Cmd + ,`), under `Build, Execution, Deployment` select `Application Servers`.
  2. Click the `+` sign and select `Tomcat Server`. 
  3. For `Tomcat Home`, navigate to the downloaded `Apache Tomcat Server 9.0.52` from the file explorer and add it. `Tomcat Base Directory` would be automatically filled by the same path as above. Press `Ok` twice.  
<br>

- **Add `Tomcat configuration` on intellij**: -
  1. Click on `Add Configurations` at the top-right on intellij and click on the `+` sign at the top left. 
  2. Scroll down and select `Tomcat Server > Local`.
  3. In the configuration menu just opened, for `JRE` select `11`. 
  4. Click on the `deployment` tab, click the `+` sign and select `Artifact` and choose `insta-response:war`. Press `OK`.
  5. Change the `application context` (at the bottom) to just `insta-response`. Click `OK`.


### Important links: 
- **[InstaResponse Project Initial Pitch Video](https://drive.google.com/file/d/1yzmkV-Bymon_T4ezkPvJtzwveFhf8owc/view?usp=sharing)**
- **[Final Deliverable Video](https://drive.google.com/file/d/1ImGQY3qTo24CeoCwK3kLb2q5txilrEi9/view?usp=sharing)**
- [Statement of Work](https://gitlab.com/JasmeenKaurGmail/instaresponse-public/-/blob/master/Assets/InstaResponse-SoW.pdf) - *also in `Assets` folder*
- [InstaResponse Wireframe Miro](https://miro.com/app/board/o9J_l3kyvSc=/) - *also in `Assets` folder*
- [Internship Schedule](https://docs.google.com/spreadsheets/d/1yQiHt0G-iNFluRf_Om8ZUfZkPDTrs3r8qUlf00dPPd0/edit#gid=398307369) - *Courtesy of Zaid Asfour*
- [Semester Roadmap link](https://fireapp-sem2-2021.atlassian.net/wiki/spaces/~147651722/pages/2557409/Deliverables+Roadmap) - *also in `Assets` folder*

### Skills and knowledge to be demonstrated and/or acquired  during the term of internship:

- Ability to gather, analyse and organise existing manual business processes into
a [component-based software design](https://en.wikipedia.org/wiki/Component-based_software_engineering).
- Ability to utilise existing web-based APIs to present a unified view of a specific 
incident.
- Knowledge and experience building a [SPA](https://en.wikipedia.org/wiki/Single-page_application) based web-application.
- Knowledge and experience with JS/HTML/CSS based software development methodologies.
- Knowledge and experience with implementing [docker-based](https://www.docker.com/why-docker) deployment strategies.


*Platform for project repo*: www.gitlab.com 

*Inspiration* - [ANU Service Desk](https://servicedesk.anu.edu.au/sp)
