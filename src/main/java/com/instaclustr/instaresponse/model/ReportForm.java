package com.instaclustr.instaresponse.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity                                             // JPA annotation, this class would be recognised as a table.
public class ReportForm {                           // ReportForm is a java class which is mapped to a database table. This will map directly to a database table named "ReportForm".
    @Id
    @GeneratedValue
    private Long id;                                // this id is the primary key which will be generated automatically by the @generatedValue annotation.

    //These properties are mapped to columns that share the same names as the properties themselves.
    @DateTimeFormat(pattern = "yyyy-MM-dd")         // This annotation is important for the Date input to work in thymeleaf! - https://stackoverflow.com/questions/38693971/input-type-date-thymeleaf
    private Date date;
    private String description;
    private String title;
    private String incidentOwner;
    private String affectedUsers;
    private String additionalInfo;

    /**
     * This is the default constructor and exists only for the sake of JPA.
     * Do not need to use it directly, hence it is designated as protected.
     */
    protected ReportForm() {}


    /**
     * Constructor to create instances of the ReportForm to be saved to database.
     */
    public ReportForm(Date date,
                      String description,
                      String title,
                      String incidentOwner,
                      String affectedUsers,
                      String additionalInfo) {
        this.date = date;
        this.description = description;
        this.title = title;
        this.incidentOwner = incidentOwner;
        this.affectedUsers = affectedUsers;
        this.additionalInfo = additionalInfo;
    }

    @Override
    public String toString() {
        return ("Issue{" + "\n"
                + "id: " + id + "\n"
                + "title: " + title + "\n"
                + "description: " + description + "\n"
                + "Incident Owner: " + incidentOwner + "\n"
                + "Date: " + date + "\n"
                + "Affected Users: " + affectedUsers + "\n"
                + "Additional Info: " + additionalInfo + "}" + "\n");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIncidentOwner() {
        return incidentOwner;
    }

    public void setIncidentOwner(String incidentOwner) {
        this.incidentOwner = incidentOwner;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAffectedUsers() {
        return affectedUsers;
    }

    public void setAffectedUsers(String affectedUsers) {
        this.affectedUsers = affectedUsers;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
