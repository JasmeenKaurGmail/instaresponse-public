package com.instaclustr.instaresponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

import java.util.Locale;

// This is the annotation for the configuration class.
@Configuration
public class InstaResponseConfig implements WebMvcConfigurer {

    @Autowired
    private ApplicationContext applicationContext;                   // applicationContext should be autoWired into our application.

    /**
     * This is for hosting static files from inside our application.
     * Check if this method (addResourceHandlers) is deprecated. FIXME
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/files/**")        // write localhost:8080/conference/files/wireframe.pdf
                .addResourceLocations("/WEB-INF/pdf/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    /**
    * Internationalisation:
    * it is going to create an instance of the locale resolver and put it out in a
    * spring registry for us. This is the session locale resolver.
    */
    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();        // SessionLocaleResolver is the one which ties our current session with the locale.
        slr.setDefaultLocale(Locale.US);                   // FIXME can change the locale based on the needs.
        return slr;
    }

    /**
     * Looks for a parameter either through a hidden element or URL string
     * to see if it should intercept that change. Here we are looking for the param
     * name called lang. So, now we just created the localeinterceptor, and now we
     * are gonna register it.
     * @return
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");                              // we are looking for the param name of "lang" in url. `?lang=es`
        return lci;
    }

    /**
     * This is how @springbootapplication resolves and displays the view resolvers.
     * This is also done by the application class, if we add jsp resolver in the application.properties file.
     * FIXME Delete this method if using only thymeleaf and get rid of the jsp poges & application.properties settings.
     * @return
     */
//    @Bean
//    public ViewResolver viewResolver() {
//        InternalResourceViewResolver bean = new InternalResourceViewResolver();
//        bean.setPrefix("/WEB-INF/jsp/");
//        bean.setSuffix(".jsp");
//        bean.setOrder(1);                                      // (In case we have multiple view resolvers) This order could be changed based on whatever we wanna look for.
//        return bean;
//    }

    /**
     * Handled in application.properties file. The ViewResolver looks up the actual template
     * and takes whichever template was loaded by the templateResolver and returns
     * that based off the name. ViewResolver and TemplateResolver kind of work in conjunction with each other.
     * @return
     */
//    @Bean
//    public ViewResolver thymeleafResolver() {
//        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
//        viewResolver.setTemplateEngine(templateEngine());
//        viewResolver.setOrder(0);                               // (In case we have multiple view resolvers) This order has to be before the JSP page (in this project for it work)
//        return viewResolver;
//    }

    /**
     * A Template resolver is not the same as the viewResolver. It is similar to ViewResolver
     * however, it does not resolve views, instead it resolves the templates. It is mainly
     * for how to locate the template files and how they should be resolved. The viewResolver just sets
     * the order and references that this templateResolver will use.
     * @return
     */
//    @Bean
//    public SpringResourceTemplateResolver templateResolver() {
//        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
//        templateResolver.setApplicationContext(applicationContext);
////        templateResolver.setPrefix("/WEB-INF/views/");           // Use this path in case thymeleaf templates are located in WEB-INF/views folder.
//        templateResolver.setPrefix("classpath:/templates/");       // classpath: at the start is important. https://stackoverflow.com/questions/43710326/spring-boot-cannot-change-thymeleaf-template-directory-with-java-config
//        templateResolver.setSuffix(".html");
//        return templateResolver;
//    }

    /**
     * TemplateEngine() utilises the templateResolver() above. This is something unique to ThymeLeaf.
     * This is a spring template engine that will process the pages and substitute in the model
     * values from spring into the pages that need to be displayed. This will process the pages and substitute
     * the model value in spring into the pages that we display.
     * @return springTemplateEngine
     */
//    @Bean
//    public SpringTemplateEngine templateEngine() {
//        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//        templateEngine.setTemplateResolver(templateResolver());             // This is calling the templateResolver() that we create above.
//        templateEngine.setEnableSpringELCompiler(true);                     // It also enabled the SpringEL compiler, which helps us to use the shorthand syntax of accessing spring variables.
//        return templateEngine;
//    }
}
