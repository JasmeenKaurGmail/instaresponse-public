package com.instaclustr.instaresponse.controller;

import com.instaclustr.instaresponse.repository.IssueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @Autowired
    IssueRepository issueRepository;
    /**
     * GET mapping of index.html page.
     * @param model An iterator of `incidents` is passed to the index.html page to display.
     * @return index.html thymeleaf template.
     */
    @RequestMapping("/")
    public String getIndex(Model model) {
        model.addAttribute("incidents", issueRepository.findAll());
        return "index";
    }
}
