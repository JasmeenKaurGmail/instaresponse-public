package com.instaclustr.instaresponse.controller;

import com.instaclustr.instaresponse.repository.IssueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class DisplayIssueController {
    @Autowired
    IssueRepository issueRepository;

    @GetMapping("displayIssue")
    public String getDisplayIssue(@ModelAttribute("displayIssue") Model model) {
        return "displayIssue";
    }
}
