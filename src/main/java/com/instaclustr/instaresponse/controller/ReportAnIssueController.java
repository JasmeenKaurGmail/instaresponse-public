package com.instaclustr.instaresponse.controller;

import com.instaclustr.instaresponse.model.ReportForm;
import com.instaclustr.instaresponse.repository.IssueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ReportAnIssueController {
    @Autowired
    IssueRepository issueRepository;

    /**
     * Model attribute - we have the object bound by our model to this model attribute.
     * @param reportForm object to store the form inputs
     * @return reportAnIssue.html thymeleaf template
     */
    @GetMapping("reportAnIssue")
    public String getReportAnIssue(@ModelAttribute("reportAnIssue") ReportForm reportForm) {
        return "reportAnIssue";
    }

    /**
     * The redirect keyword works for pretty much for any view resolver including thymeleaf.
     * @param reportForm object to store the form inputs
     * @return
     */
    @PostMapping("reportAnIssue")
    public String postReportAnIssue(@ModelAttribute("reportAnIssue") ReportForm reportForm, Model model) {
        System.out.println("Incident added through post: ");
        System.out.println(reportForm.toString());

        issueRepository.save(reportForm);                           // add the incident to the database

        //print all the incidents of the database ------------
        System.out.println("*********************************");
        System.out.println("Issues found with findAll():");
        System.out.println("----------------------------");
        for (ReportForm issue : issueRepository.findAll()) {
            System.out.print(issue.toString());
            System.out.println();
        }
        System.out.println();
        System.out.println("*********************************");
        // --------------------------------------

        model.addAttribute("reportForm", reportForm);
        return "submitIssue";                                             // Display the submitIssue page.
    }
}
