package com.instaclustr.instaresponse.repository;

import com.instaclustr.instaresponse.model.ReportForm;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * By extending Paging and Sorting Repository, it inherits several methods such as
 * creating, reading, updating, deleting and other paging and sorting methods.
 * Do not need to write an implementation for this interface, as spring data JPA
 * creates an implementation when it is run.
 */
public interface IssueRepository extends PagingAndSortingRepository<ReportForm, Long> {
    // Other query methods declared below.

    /**
     * This returns a list of all the incidents with the same title.
     * @param title title of the incident
     * @return List of all the incidents with given title.
     */
    List<ReportForm> findByTitle(String title);

    /**
     * Finds the issue by ID.
     * @param id input ID.
     * @return Issue with the same ID.
     */
    ReportForm findById(long id);

}
