package com.instaclustr.instaresponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class InstaResponseApplication extends SpringBootServletInitializer {    // basically launches stuff that we want to - this servlet initializer act as a Resolver, like resolving thymeleaf pages.

	//This main method is executed when the application starts.
	public static void main(String[] args) {
		SpringApplication.run(InstaResponseApplication.class, args);
	}
}
